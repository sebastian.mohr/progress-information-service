package de.smohr.progressinformationservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class Progressbar {

    private String id;
    private String description;

    @Override
    public String toString() {
        return String.format("Progressbar[id=%s]", id);
    }

}
