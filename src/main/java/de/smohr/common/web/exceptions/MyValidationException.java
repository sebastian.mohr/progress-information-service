package de.smohr.common.web.exceptions;

public class MyValidationException extends RuntimeException {

    public MyValidationException(String message) {
        super(message);
    }
}
