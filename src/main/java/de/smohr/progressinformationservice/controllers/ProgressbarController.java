package de.smohr.progressinformationservice.controllers;

import de.smohr.common.web.exceptions.MyResourceNotFoundException;
import de.smohr.progressinformationservice.model.Progressbar;
import de.smohr.progressinformationservice.model.ProgressbarMapper;
import de.smohr.progressinformationservice.resources.ProgressbarResource;
import de.smohr.progressinformationservice.service.ProgressbarService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static net.logstash.logback.argument.StructuredArguments.kv;

@Slf4j
@RestController
@RequestMapping("/v1/progressbar")
@AllArgsConstructor
public class ProgressbarController {

    private ProgressbarMapper progressbarMapper;
    private ProgressbarService progressbarService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void erstelleNeueProgressbar(@Valid @RequestBody final ProgressbarResource progressbarResource) {
        log.debug("Erstelle neue Progressbar: {}", kv("id", progressbarResource.getId()));
        progressbarService.erstelleNeueProgressbar(progressbarMapper.mapResourceIntoModel(progressbarResource));
    }

    @GetMapping
    public List<ProgressbarResource> ermittelAlleProgressbars() {
        log.debug("Ermittel alle Progressbars");
        return progressbarService.holeLimitierteProgressbarsSortiertNachIdAufsteigend()
                    .stream()
                    .map(progressbar -> progressbarMapper.mapModelIntoResource(progressbar))
                    .collect(Collectors.toList());
    }

    @GetMapping(params = QueryConstants.LIMIT)
    public List<ProgressbarResource> ermittelLimitierteProgressbars(
            @RequestParam(value = QueryConstants.LIMIT) final int limit) {
        log.debug("Ermittel die letzten {} Progressbars", limit);
        return progressbarService.holeLimitierteProgressbarsSortiertNachIdAufsteigend(limit)
                .stream()
                .map(progressbar -> progressbarMapper.mapModelIntoResource(progressbar))
                .collect(Collectors.toList());
    }

    @GetMapping(value =  "/{id}")
    public ProgressbarResource ermittelProgressbarById(@PathVariable final String id) {
        log.debug("Ermittel die Progressbar mit der Id {}", id);
        Optional<Progressbar> progressbar = progressbarService.holeProgressbarById(id);
        return progressbarMapper.mapModelIntoResource(progressbar.orElseThrow(() ->
                new MyResourceNotFoundException(Progressbar.class, id)));
    }

    @DeleteMapping(value = "/{id}")
    public void loescheProgressbarById(@PathVariable final String id) {
        log.debug("Lösche Progressbar mit der Id {}", id);
        progressbarService.loescheProgressbarById(id);
    }


    @PutMapping(value = "/{id}")
    public void updateProgressbarById(@PathVariable final String id,
                                       @RequestBody ProgressbarResource progressbarResource) {
        log.debug("Update bestehende Progressbar mit der Id {}", id);
        progressbarService.updateProgressbarById(id, progressbarMapper.mapResourceIntoModel(progressbarResource));
    }
}
