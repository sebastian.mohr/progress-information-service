package de.smohr.progressinformationservice.resources;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class ProgressbarResource {

    @NotEmpty
    private String id;

    private String description;

}

