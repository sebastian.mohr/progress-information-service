package de.smohr.common.web.exceptions;

public class MyResourceNotFoundException extends RuntimeException {

    public MyResourceNotFoundException(Class c, String id) {
        super(String.format("Die Resource %s:%s wurde nicht gefunden.", c.getSimpleName(), id));
    }
}
