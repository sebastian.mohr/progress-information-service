package de.smohr.progressinformationservice;

import de.smohr.common.web.configuration.WebConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(WebConfiguration.class)
public class ProgressInformationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProgressInformationServiceApplication.class, args);
	}

}
