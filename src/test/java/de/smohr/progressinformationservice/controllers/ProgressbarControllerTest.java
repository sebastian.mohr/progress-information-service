package de.smohr.progressinformationservice.controllers;

import de.smohr.common.web.exceptions.MyResourceNotFoundException;
import de.smohr.progressinformationservice.model.Progressbar;
import de.smohr.progressinformationservice.model.ProgressbarMapper;
import de.smohr.progressinformationservice.resources.ProgressbarResource;
import de.smohr.progressinformationservice.service.ProgressbarService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Collections;
import java.util.Optional;

import static com.atlassian.oai.validator.mockmvc.OpenApiValidationMatchers.openApi;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ProgressbarController.class)
class ProgressbarControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProgressbarService progressbarService;

    @MockBean
    private ProgressbarMapper progressbarMapper;

    @BeforeEach
    void setUp() {
        when(progressbarMapper.mapResourceIntoModel(any(ProgressbarResource.class)))
                .thenReturn(Progressbar.builder().build());
    }

    @Test
    void givenCorrectProgressbarParam_whenPost_thenReturn201() throws Exception {
        //given
        String requestBody = "{" +
                "\"id\": \"Test\"" +
                "}";

        //when
        ResultActions result = mockMvc.perform(post("/v1/progressbar")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(requestBody));

        //then
        result.andExpect(status().isCreated())
                .andExpect(openApi().isValid("/static/openapi.yml"));
    }

    @Test
    void givenProgressbarWithUnknownParam_whenPost_thenReturn400() throws Exception {
        //given
        String requestBody = "{" +
                "\"id\": \"Test\"" +
                "\"unknownKey\": \"unkownValue\"" +
                "}";

        //when
        ResultActions result = mockMvc.perform(post("/v1/progressbar")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(requestBody));

        //then
        result.andExpect(status().isBadRequest());
        result.andExpect(content()
                .json("{'status': 400}"));
    }

    @Test
    void givenOneExistingProgressbar_whenGet_thenReturnExistingProgressbar() throws Exception {
        // given
        ProgressbarResource progressbarResource = new ProgressbarResource();
        progressbarResource.setId("test");
        when(progressbarService.holeLimitierteProgressbarsSortiertNachIdAufsteigend()).thenReturn(
                Collections.singletonList(Progressbar.builder().id("test").build()));
        when(progressbarMapper.mapModelIntoResource(any(Progressbar.class))).thenReturn(progressbarResource);


        // when
        ResultActions result = mockMvc.perform(get("/v1/progressbar")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8"));

        // then
        result
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json("[{'id': 'test'}]"))
                .andExpect(openApi().isValid("/static/openapi.yml"));
    }


    @Test
    void givenNoneExistingProgressbar_whenGet_thenReturn2xxAndEmptyArray() throws Exception {
        // when
        ResultActions result = mockMvc.perform(get("/v1/progressbar")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8"));

        // then
        result
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json("[]"))
                .andExpect(openApi().isValid("/static/openapi.yml"));
    }

    @Test
    void whenGetWithQueryParamLimit_openApiIsValid() throws Exception {
        // when
        ResultActions result = mockMvc.perform(get("/v1/progressbar")
                .queryParam("limit", "1")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8"));

        // then
        result
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json("[]"))
                .andExpect(openApi().isValid("/static/openapi.yml"));
    }

    @Test
    void givenExistingProgressbar_whenGetWithSpecificId_thenReturn2xxAndExistingProgressbar() throws Exception {
        // given
        ProgressbarResource progressbarResource = new ProgressbarResource();
        progressbarResource.setId("testPB");
        when(progressbarService.holeProgressbarById(anyString())).thenReturn(Optional.ofNullable(
                Progressbar.builder().id("testPB").build()));
        when(progressbarMapper.mapModelIntoResource(any(Progressbar.class))).thenReturn(progressbarResource);

        // when
        ResultActions result = mockMvc.perform(get("/v1/progressbar/testPB"));

        // then
        result
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().json("{'id': 'testPB'}"))
                .andExpect(openApi().isValid("/static/openapi.yml"));
    }

    @Test
    void givingIdOhneExistierendeProgressbar_whenGetMitId_thenThrow404() throws Exception {
        //when
        ResultActions result = mockMvc.perform(get("/v1/progressbar/notExisting"));

        //then
        result.andExpect(status().isNotFound());
        result.andExpect(content()
                .json("{" +
                        "'status': 404, " +
                        "'message': 'Die Resource Progressbar:notExisting wurde nicht gefunden.'" +
                        "}"))
                .andExpect(openApi().isValid("/static/openapi.yml"));

    }

    @Test
    void givenExistingProgressbar_whenDeleteWithSpecificId_thenReturn2xxAndDeletedProgressbar() throws Exception {
        // when
        ResultActions result = mockMvc.perform(delete("/v1/progressbar/testPB"));

        // then
        result
                .andExpect(status().is2xxSuccessful())
                .andExpect(openApi().isValid("/static/openapi.yml"));
    }


    @Test
    void givenNoneExistingProgressbar_whenDeleteWithSpecificId_thenReturn404() throws Exception {
        //given
        doThrow(new MyResourceNotFoundException(Progressbar.class, "NoneExisting"))
                .when(progressbarService)
                .loescheProgressbarById(anyString());

        // when
        ResultActions result = mockMvc.perform(delete("/v1/progressbar/testPB"));

        // then
        result
                .andExpect(status().isNotFound())
                .andExpect(content()
                        .json("{" +
                                "'status': 404, " +
                                "'message': 'Die Resource Progressbar:NoneExisting wurde nicht gefunden.'" +
                                "}"))
                .andExpect(openApi().isValid("/static/openapi.yml"));
    }

    @Test
    void givenExistingProgressbar_whenPutWithSpecificId_thenReturn200() throws Exception {
        //given
        String requestBody = "{" +
                "\"id\": \"changedId\"" +
                "}";

        //when
        ResultActions result = mockMvc.perform(put("/v1/progressbar/id")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(requestBody));

        //then
        result.andExpect(status().is2xxSuccessful())
                 .andExpect(openApi().isValid("/static/openapi.yml"));
    }

    @Test
    void givenNoneExistingProgressbar_whenPutWithSpecificId_thenReturn404() throws Exception {
        //given
        String requestBody = "{" +
                "\"id\": \"changedId\"" +
                "}";
        doThrow(new MyResourceNotFoundException(Progressbar.class, "NoneExisting"))
                .when(progressbarService)
                .updateProgressbarById(anyString(), any(Progressbar.class));

        // when
        //when
        ResultActions result = mockMvc.perform(put("/v1/progressbar/NoneExisting")
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(requestBody));
        // then
        result
                .andExpect(status().isNotFound())
                .andExpect(content()
                        .json("{" +
                                "'status': 404, " +
                                "'message': 'Die Resource Progressbar:NoneExisting wurde nicht gefunden.'" +
                                "}"))
                .andExpect(openApi().isValid("/static/openapi.yml"));
    }

}