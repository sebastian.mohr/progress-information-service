package de.smohr.progressinformationservice.service;

import de.smohr.common.web.exceptions.MyDuplicateResourceException;
import de.smohr.common.web.exceptions.MyResourceNotFoundException;
import de.smohr.common.web.exceptions.MyValidationException;
import de.smohr.progressinformationservice.model.Progressbar;
import de.smohr.progressinformationservice.repositories.ProgressbarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProgressbarService {

    private final ProgressbarRepository progressbarRepository;

    @Autowired
    public ProgressbarService(ProgressbarRepository progressbarRepository) {
        this.progressbarRepository = progressbarRepository;
    }

    public void erstelleNeueProgressbar(Progressbar progressbar) {
        if(progressbarRepository.findById(progressbar.getId()).isPresent()) {
            throw new MyDuplicateResourceException(progressbar);
        }

        progressbarRepository.insert(progressbar);
    }

    public List<Progressbar> holeLimitierteProgressbarsSortiertNachIdAufsteigend() {
        return progressbarRepository.findAll();
    }

    public List<Progressbar> holeLimitierteProgressbarsSortiertNachIdAufsteigend(int limit) {
        Page<Progressbar> progressbarPage = progressbarRepository
                .findAll(PageRequest.of(0, limit, Sort.by("id")));

        return progressbarPage.getContent();
    }

    public Optional<Progressbar> holeProgressbarById(String id) {
        return progressbarRepository.findById(id);
    }

    public void loescheProgressbarById(String id) {
        Optional<Progressbar> progressbarOptional = progressbarRepository.findById(id);
        progressbarOptional.orElseThrow(() ->
                new MyResourceNotFoundException(Progressbar.class, id));
        progressbarOptional.ifPresent(progressbarRepository::delete);
    }

    public void updateProgressbarById(String id, Progressbar progressbarForUpdate) {
        if(!id.equals(progressbarForUpdate.getId())) {
            throw new MyValidationException(
                    "Die Id der übermittelten Progressbar muss mit der angefragten Ressource zusammenpassen");
        }

        Optional<Progressbar> progressbarOptional = progressbarRepository.findById(progressbarForUpdate.getId());
        progressbarOptional.orElseThrow(() -> new MyResourceNotFoundException(Progressbar.class, id));

        progressbarRepository.save(progressbarForUpdate);
    }
}
