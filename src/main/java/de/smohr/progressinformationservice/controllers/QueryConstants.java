package de.smohr.progressinformationservice.controllers;

final class QueryConstants {

    static final String LIMIT = "limit";

    private QueryConstants() {
        throw new AssertionError();
    }

}
