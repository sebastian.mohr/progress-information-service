package de.smohr.progressinformationservice.model;

import de.smohr.progressinformationservice.resources.ProgressbarResource;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ProgressbarMapperTest {

    private ProgressbarMapper progressbarMapper;

    @BeforeEach
    void setUp() {
        progressbarMapper = new ProgressbarMapper();
    }

    @Test
    void givenProgressbarResource_whenResourceShouldBeMappedIntoModel_thenReturnModel() {
        //given
        ProgressbarResource progressbarResource = new ProgressbarResource();
        progressbarResource.setId("id");

        //when
        Progressbar progressbar = progressbarMapper.mapResourceIntoModel(progressbarResource);

        //then
        assertEquals(progressbarResource.getId(), progressbar.getId());
    }

    @Test
    void givenProgressbar_whenModelShouldBeMappedToResource_thenReturnResource() {
        //given
        Progressbar progressbar = Progressbar.builder().id("test").build();

        //when
        ProgressbarResource progressbarResource = progressbarMapper.mapModelIntoResource(progressbar);

        //then
        assertEquals(progressbar.getId(), progressbarResource.getId());
    }

}