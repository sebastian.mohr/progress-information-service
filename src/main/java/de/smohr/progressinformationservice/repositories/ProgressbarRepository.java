package de.smohr.progressinformationservice.repositories;

import de.smohr.progressinformationservice.model.Progressbar;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProgressbarRepository  extends MongoRepository<Progressbar, String> {

}
