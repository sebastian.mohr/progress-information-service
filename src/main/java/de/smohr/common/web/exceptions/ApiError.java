package de.smohr.common.web.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApiError {

    private int status;
    private String message;

    @Override
    public final String toString() {
        return "ApiError [status=" +
                status +
                ", message=" +
                message +
                "]";
    }
}
