package de.smohr.progressinformationservice.model;

import de.smohr.progressinformationservice.resources.ProgressbarResource;
import org.springframework.stereotype.Component;

@Component
public class ProgressbarMapper {

    public Progressbar mapResourceIntoModel(ProgressbarResource progressbarResource) {
        return Progressbar.builder().id(progressbarResource.getId()).build();
    }

    public ProgressbarResource mapModelIntoResource(Progressbar progressbar) {
        ProgressbarResource progressbarResource = new ProgressbarResource();
        progressbarResource.setId(progressbar.getId());

        return progressbarResource;
    }

}
