package de.smohr.progressinformationservice.service;

import de.smohr.common.web.exceptions.MyDuplicateResourceException;
import de.smohr.common.web.exceptions.MyResourceNotFoundException;
import de.smohr.common.web.exceptions.MyValidationException;
import de.smohr.progressinformationservice.model.Progressbar;
import de.smohr.progressinformationservice.repositories.ProgressbarRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ProgressbarServiceTest {

    private ProgressbarService progressbarService;
    private ProgressbarRepository progressbarRepository;

    @BeforeEach
    void setUp() {
        progressbarRepository = mock(ProgressbarRepository.class);
        progressbarService = new ProgressbarService(progressbarRepository);
    }

    @Test
    void givenProgressbar_whenErstelleNeueProgressbar_thenRepositoryIsCalled() {
        //given
        Progressbar progressbar = Progressbar.builder().id("Test").build();

        //when
        progressbarService.erstelleNeueProgressbar(progressbar);

        //then
        verify(progressbarRepository).insert(any(Progressbar.class));
    }


    @Test
    void givenProgressbar_whenErstelleNeueProgressbarWithExistingId_thenthrowsDublicateResourceException() {
        //given
        Progressbar progressbar = Progressbar.builder().id("Existing").build();

        //when
        when(progressbarRepository.findById(eq(progressbar.getId())))
                .thenReturn(Optional.ofNullable(Progressbar.builder().id("Existing").build()));

        //then
        MyDuplicateResourceException myDuplicateResourceException = assertThrows(MyDuplicateResourceException.class, () ->
                progressbarService.erstelleNeueProgressbar(progressbar));
        assertEquals("Die Resource Progressbar existiert bereits", myDuplicateResourceException.getMessage());
    }

    @Test
    void givenExistingProgressbarById_whenHoleProgressbarById_thenReturnExistingProgressbar() {
        //given
        Progressbar progressbar = Progressbar.builder().id("Existing").build();
        when(progressbarRepository.findById("Existing")).thenReturn(Optional.ofNullable(progressbar));

        //when
        Optional<Progressbar> existing = progressbarService.holeProgressbarById("Existing");

        //then
        assertTrue(existing.isPresent());
        assertEquals("Existing", existing.get().getId());
    }

    @Test
    void givenExistingProgressbarById_whenDeleteProgressbarById_thenDelete() {
        //given
        Progressbar progressbar = Progressbar.builder().id("Existing").build();
        when(progressbarRepository.findById(progressbar.getId())).thenReturn(Optional.of(progressbar));

        //when
        progressbarService.loescheProgressbarById("Existing");

        //then
        verify(progressbarRepository).delete(eq(progressbar));
    }


    @Test
    void givenNoProgressbarById_whenDeleteProgressbarById_thenThrowMyResourceNotFoundException() {
        MyResourceNotFoundException myResourceNotFoundException = assertThrows(MyResourceNotFoundException.class, () ->
                progressbarService.loescheProgressbarById("NoneExisting"));
        assertEquals("Die Resource Progressbar:NoneExisting wurde nicht gefunden.",
                myResourceNotFoundException.getMessage());
    }


    @Test
    void givenIdVonExistierenderProgressbar_whenUpdateProgressbar_thenUpdateWirdDurchgefuehrt() {
        //given
        when(progressbarRepository.findById("id"))
                .thenReturn(Optional.ofNullable(Progressbar.builder().id("id").build()));

        //when
        progressbarService.updateProgressbarById("id", Progressbar.builder().id("id").build());

        verify(progressbarRepository).save(any(Progressbar.class));
    }

    @Test
    void givenIdVonExistierenderProgressbarUndVersucheDieIdzuVeraendern_whenUpdateProgressbar_thenThrowValidationError() {
        //given
        when(progressbarRepository.findById("id"))
                .thenReturn(Optional.ofNullable(Progressbar.builder().id("id").build()));

        //then
        assertThrows(MyValidationException.class, () ->
            progressbarService.updateProgressbarById("id", Progressbar.builder().id("otherid").build()));
    }

    @Test
    void givenIdVonNichtExistierenderProgressbar_whenUpdateProgressbar_thenThrowResourceNotFoundException() {
        assertThrows(MyResourceNotFoundException.class, () ->
                progressbarService.updateProgressbarById("id", Progressbar.builder().id("id").build()));
    }
}