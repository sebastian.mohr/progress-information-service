package de.smohr.common.web.configuration;

import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;
import java.util.Optional;

@Configuration
@ComponentScan({ "de.smohr.common.web"})
public class WebConfiguration implements WebMvcConfigurer {

    /**
     * Anbei werden zwei Verhaltensweisen für alle Rest-Endpunkte definiert:
     *
     * 1. Setzen eines default Pretty Printer
     * 2. Alle Rest Schnittstellen, sollen einen Fehler werden, wenn Properties übergeben werden,
     *    die nicht explizit in den Requestobjekten definiert sind.
     *
     * @param converters: HttpMessageConverter
     */
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        Optional<HttpMessageConverter<?>> httpMessageConverter =
                converters.stream().filter(c -> c instanceof MappingJackson2HttpMessageConverter).findFirst();
        if(httpMessageConverter.isPresent()) {
            AbstractJackson2HttpMessageConverter converter =
                    (AbstractJackson2HttpMessageConverter) httpMessageConverter.get();
            converter.getObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        }
    }
}
