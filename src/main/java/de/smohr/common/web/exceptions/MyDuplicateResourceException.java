package de.smohr.common.web.exceptions;

public class MyDuplicateResourceException extends RuntimeException {

    public MyDuplicateResourceException(Object o) {
        super(String.format("Die Resource %s existiert bereits", o.getClass().getSimpleName()));
    }
}
